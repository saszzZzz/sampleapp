import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  error: string;
  info: string;
  constructor() { }

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.form = new FormGroup({
      'username':new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required),
    });
  }
  validateForm(){
    if(this.form.controls.username.errors){
      this.error = "Required username";
    }
    if(this.form.controls.password.errors){
      this.error = "Required password";
    }
  }

  login(){
    this.validateForm();
  }

}
